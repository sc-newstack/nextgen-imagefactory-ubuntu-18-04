// build {
//   sources = ["source.azure-arm.windows"]

//   provisioner "powershell" {
//     inline = ["Add-WindowsFeature Web-Server", "while ((Get-Service RdAgent).Status -ne 'Running') { Start-Sleep -s 5 }", "while ((Get-Service WindowsAzureGuestAgent).Status -ne 'Running') { Start-Sleep -s 5 }", "& $env:SystemRoot\\System32\\Sysprep\\Sysprep.exe /oobe /generalize /quiet /quit", "while($true) { $imageState = Get-ItemProperty HKLM:\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Setup\\State | Select ImageState; if($imageState.ImageState -ne 'IMAGE_STATE_GENERALIZE_RESEAL_TO_OOBE') { Write-Output $imageState.ImageState; Start-Sleep -s 10  } else { break } }"]
//   }

// }

build {
   hcp_packer_registry {
    bucket_name = "burkey-golden-ubuntu-1804"
    description = <<EOT
Ubuntu 18.04 golden master.
    EOT
    bucket_labels = {
      "ubuntu" = "18.04-2022",
      "golden"         = "true",
      "security_hardened" = "true",
      "cis_benchmarked" = "true",
      "github_action" = "true",
    }
    build_labels = {
        "log4j" = "resolved"
        "ciphers" = "2022-approved"
        "python-version"   = "3.9",
        "ubuntu-version" = "20.04.03"
        "build-time" = timestamp()
}
  }
  sources = ["source.amazon-ebs.base"]
}