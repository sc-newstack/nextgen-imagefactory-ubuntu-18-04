locals {
	timestamp = regex_replace(timestamp(), "[- TZ:]", "")
	// vcenter_user = vault("/ad/creds/packer", "username")
	// vcenter_password = vault("/ad/creds/packer", "current_password")
  // vcenter_server = "hlcorevc01.humblelab.com"
}

source "amazon-ebs" "base" {
  region = var.region

	// vault_aws_engine {
	// 	name = "packer"
	// 	engine_name = "aws"
	// 	role_arn = "arn:aws:iam::711129375688:role/packer"
	// 	ttl = "30m"
	// }

  source_ami_filter {
    filters = {
       virtualization-type = "hvm"
       name = "ubuntu/images/*ubuntu-bionic-18.04-amd64-server-*"
       root-device-type = "ebs"
    }
    owners = ["099720109477"] #Canonical
    most_recent = true
  }

  instance_type = "t2.medium"
  ssh_username = "ubuntu"
  ami_name = "${var.prefix}-${local.timestamp}"

  tags = {
    owner = var.owner
    application = "base-build"
    Base_AMI_Name = "{{ .SourceAMIName }}"
  }
}

source "vsphere-iso" "ubuntu-1804" {
  vcenter_server = local.vcenter_server
  username = format("%s%s%s", local.vcenter_user, "@", var.domain)
  password = local.vcenter_password
  insecure_connection = var.vcenter_insecure_connection

  vm_name   = "${var.vm_name}-${local.timestamp}"
  cluster   = var.cluster
  host      = var.host
  datastore = var.datastore

  ssh_username = "ubuntu"
  ssh_password = "Hashi123!"
  ssh_pty      = true

  guest_os_type = var.guest_os_type

  CPUs = 2
  RAM  = 4096

  RAM_reserve_all      = true
  disk_controller_type = [ "pvscsi" ]

  storage {
    disk_size =        32768
    disk_thin_provisioned = true
  }

  network_adapters {
    network = var.network
    network_card = "vmxnet3"
  }

  iso_paths = var.iso_paths
  floppy_files = [
    var.floppy_path
  ]
  boot_command = [<<EOF
<enter><wait><f6><wait><esc><wait>
<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>
<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>
<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>
<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>
<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>
<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>
<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>
<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>
<bs><bs><bs>
/install/vmlinuz initrd=/install/initrd.gz priority=critical locale=en_US file=/media/preseed.cfg
<enter>
EOF
  ]
}

// source "azure-arm" "windows" {
//   azure_tags = {
//     dept = "Engineering"
//     task = "Image deployment"
//   }
//   build_resource_group_name         = "myPackerGroup"
//   communicator                      = "winrm"
//   image_offer                       = "WindowsServer"
//   image_publisher                   = "MicrosoftWindowsServer"
//   image_sku                         = "2016-Datacenter"
//   managed_image_name                = "myPackerImage"
//   managed_image_resource_group_name = "myPackerGroup"
//   os_type                           = "Windows"
//   vm_size                           = "Standard_D2_v2"
//   winrm_insecure                    = true
//   winrm_timeout                     = "5m"
//   winrm_use_ssl                     = true
//   winrm_username                    = "packer"
// }